# Launch Nmap jobs in K8s

## Test run with podman

* Create your own env file like ``cp env.default.sh env.dev.sh``.

    $ podman run --cap-add NET_RAW -v "$(pwd)/env.dev.sh:/app/env.default.sh" localhost/nmap:master /app/run_nmap.sh example.com

## Run in kubernetes

Coming soon.
