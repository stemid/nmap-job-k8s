#!/usr/bin/env bash

export BUILDAH_FORMAT=docker

set -o errexit

from_tag=docker.io/alpine:3.12

# Use some environment variables here so we can provide them in CI pipelines.
image_name=${IMAGE_NAME:-nmap}
container=$(buildah from $from_tag)
registry_image=${REGISTRY_IMAGE:-"registry.hub.docker.com/stemid/$image_name"}
registry_tag=${REGISTRY_TAG:-master}

test -f "$REGISTRY_AUTH_JSON" && cp -v "$REGISTRY_AUTH_JSON" ./auth.json

buildah config --label maintainer="Stefan Midjich <swehack at gmail.com>" $container
buildah config --workingdir /app $container

buildah run $container apk -U upgrade
buildah run $container apk --update-cache add nmap nmap-scripts aws-cli
buildah copy $container ./run_nmap.sh /app/run_nmap.sh
buildah copy $container ../env.default.sh /app/env.default.sh
buildah run $container chmod +x /app/run_nmap.sh

echo "$0: buildah commit"
buildah commit $container $image_name:$registry_tag

