#!/usr/bin/env sh

subnet=${NMAP_SCAN_SUBNET:-"$1"}
env=${NMAP_SCAN_ENV:-default}
currentTimestamp=$(date +'%Y%m%d.%H%M%S.%s')

# Remote /CIDR suffix from subnet to use as filename
outputFile="${subnet%%/??}-$currentTimestamp.xml"

test -z "$subnet" && exit 1
test -r "env.$env.sh" && source "env.$env.sh"

nmap -p 1-65535 -A -Pn -oX "$outputFile" "$subnet"
test -r "$outputFile" aws s3 cp "$outputFile" "s3://$AWS_BUCKET/$outputFile"
